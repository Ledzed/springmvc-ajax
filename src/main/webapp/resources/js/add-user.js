$(document).ready(function($){
	$("#add_user_form").submit(function(event){
		event.preventDefault();
		postUserRegistration();
	});
	$("#search_stripe").submit(function(event){
		event.preventDefault();
		doNewSearch();
	});
});

function postUserRegistration(){
	var user = {
			'arg_1': $('#username').val(),
			'arg_2': $('#password').val(),
			'arg_3': $('#email').val(),
			'arg_4': $('#phone').val(),
			'arg_5': $('#address').val()
	};
	$.ajax({
		type: 'POST',
		contentType: 'application/json',
		timeout: 2000,
		url: 'add_user',
		data: JSON.stringify(user),
		success: function(data){
			if(data === 'ok'){
				$('#username').empty();
				$('#password').empty();
				$('#email').empty();
				$('#phone').empty();
				$('#address').empty();
				doNewSearch();
			}
		}
	});
}

function doNewSearch(){
	var username = $('#search_name').val();
	$.ajax({
		type: 'POST',
		contentType: 'text/plain',
		timeout: 2000,
		url: 'search_user',
		data: JSON.stringify(username),
		success: function(data){
			displaySearchResult(data);
		},
		error: function (e){
			console.log("ERROR: ", e);
			displaySearchResult(e);
		}
	});
}

function displaySearchResult(result){
	var resField = $('#search_result');
	resField.empty();
	$.each(result, function(key, value){
		resField.append("KEY: "+key+"  VALUE: "+value+"\n");
		goDeepToObject(resField,value);
//		resField.append("");
	});
	//resField.append(JSON.stringify(result, null, 4));
}

function goDeepToObject(fieldDOM, obj){
	if($.type(obj) === "array" || $.type(obj) === "object"){
		$.each(obj, function(key, value){
			fieldDOM.append(key+" -> ");
			goDeepToObject(fieldDOM,value);
		});
	}else{
		fieldDOM.append(obj + "\n");
	}
	fieldDOM.append("\n");
}