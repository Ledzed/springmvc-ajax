function getData(){
	var xhttp = new XMLHttpRequest();
	//xhttp.open("POST","do_action",true);
	xhttp.open("GET", "resources/text/text.txt", true);
	xhttp.send();
	xhttp.onreadystatechange = function(){
		if(this.readyState == 4 && this.status == 200){
			$("#piy").empty();
			$("#piy").append(this.responseText);
		}
	}
}

function CalculateThis(){
	var inputData = $('#calc_this').val();
	$.ajax({
		type: 'POST',
		contentType: 'text/plain',
		url: 'calc_this',
		data: (inputData),
		timeout: 10000,
		success: function(data){
			$('#calc_result').empty();
			$('#calc_result').append(data);
		}
	});
}

$(document).ready(function($){
	$("#search-form").submit(function(event){
		enableSearchButton(false);
		event.preventDefault();
		searchViaAjax();
	});
	
	$("#simple-calc").submit(function(event){
		event.preventDefault();
		doCalc();
	});
	
	$('#ajax-calc').submit(function(event){
		event.preventDefault();
		calcViaAjax();
	});

});

function calcViaAjax(){
	var calcData = {};
	calcData["arg_1"] = Number($('#a_arg_1').val());
	calcData["arg_2"] = Number($('#a_arg_2').val());
	$.ajax({
		type:        'POST',
		contentType: 'application/json',
		url:         'calc',
		data:        JSON.stringify(calcData),
		timeout:     2000,
		success:		
			function(data){
				$('#a_summ').empty();
				$('#a_summ').append(JSON.stringify(data, null, 4));
			}
	});
}

function doCalc(){
	var summ = Number($('#arg_1').val()) + Number($('#arg_2').val());
	var answer = $('#summ');
	answer.empty();
	answer.append(summ);
}

function searchViaAjax(){
	
	var search = $("#search").val();
	
	$.ajax({
		type: 			"POST",
		contentType:	"application/json",
		url:			"do",
		data: 			JSON.stringify(search),
		dataType: 		"json",
		timeout: 		100000,
		success: function(data){
			console.log("SUCCESS: ", data);
			display(data);
		},
		error: function (e){
			console.log("ERROR: ", e);
			display(e);
		},
		done: function(e){
			console.log("DONE");
		}
	});
}

function enableSearchButton(flag){
	$('#btn-search').prop('disabled',flag);
}

function display(data){
	var json = "<h4>Ajax Response</h4><pre>" 
				+ JSON.stringify(data, null, 4) + "</pre>";
	$('#feedback').html(json);
}

