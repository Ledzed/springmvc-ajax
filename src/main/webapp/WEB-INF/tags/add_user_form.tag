<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ attribute name="temp" required="false" %>

<form id='add_user_form'>
	<div><label>User name:</label><input type='text' id='username' required="required"/></div>
	<div><label>Password:</label><input type="password" id='password' required="required"/></div>
	<div><label>E-mail:</label><input type="email" id='email' required="required"/></div>
	<div><label>Phone:</label><input type='text' id='phone'/></div>
	<div><label>Address:</label><input type='text' id='address'/></div>
	<div><input type='submit' value='add user'></div>
</form>