<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="t" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="myTag" tagdir="/WEB-INF/tags" %>

<html>
	<head>
		<t:url value="/resources/css/user.css" var="css_user"/>
		<t:url value="/resources/js/jquery-3.2.1.min.js" var="jquery"/>
		<t:url value="/resources/js/add-user.js" var="addUser"/>

		<script src="${jquery}"></script>
		<script src="${addUser}"></script>
	
		<link rel="stylesheet" type="text/css" href='${css_user}'>
	
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>USERS</title>
	</head>
	<body>
		<header>
			Header
		</header>
		<nav>
			nav tag
		</nav>
		<section id="main">
			<section>
				<myTag:add_user_form/>
				<myTag:search_stripe temp="temp_text"/>
			</section>
			<aside>
				<textarea id="search_result">
				</textarea>
			</aside>
		</section>
		<footer>
			footer
		</footer>
	</body>
</html>

