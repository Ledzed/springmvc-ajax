<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="t" uri="http://www.springframework.org/tags" %>
<%@ page session="false" %>

<html>
<head>
	<t:url value="/resources/css/main.css" var="css_main"/>
	<t:url value="/resources/js/jquery-3.2.1.min.js" var="jquery"/>
	<t:url value="/resources/js/ajax.my.js" var="ajax_my"/>
	<!-- <script type="text/javascript" src='<c:url value="/resources/js/jquery-3.2.1.min.js"/>'></script> -->
	<script src="${jquery}"></script>
	<script src="${ajax_my}"></script>
	
	<link rel="stylesheet" type="text/css" href='${css_main}'>
	
	<title>Home</title>
</head>
<body>
	<h1>Hi!</h1>
	<a href='<c:url value='/users'/>'>go to users page</a>
	<P>  The time on the server is ${serverTime}. </P>
	
	<input type='text' id='calc_this'/><br/>
	<button id='calculate_this' onclick='CalculateThis()'>Calculate</button>
	<p id='calc_result'></p>
	<hr>
	
	<form id="simple-calc">
		<h2>jQuery simple calc</h2>
		<input type="number" id="arg_1"/>&nbsp;+&nbsp;
		<input type="number" id="arg_2"/>
		<input type="submit">
	</form>
	<span>Summ: <label id="summ"></label></span>
	<hr>
	
	<form id="ajax-calc">
		<h2>Ajax simple calc</h2>
		<input type="number" id="a_arg_1"/>&nbsp;*2&nbsp;+&nbsp;
		<input type="number" id="a_arg_2"/>
		<input type="submit" id="btn-summ">
	</form>
	<span>Summ: <label id="a_summ"></label></span>
	<hr>
	
		<button id='btn-search' type="button" onclick="getData()">Click me!</button>
		<span id="piy"></span>
	<hr>
	
	<form id="search-form">
		<input id='search' type="text"/>
		<input id="btn-search" type="submit">
	</form>
	<div id='feedback'></div>

</body>
</html>
