package c.o.m.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.annotation.JsonView;

import c.o.m.Views;
import c.o.m.model.AjaxResponseBody;
import c.o.m.model.CalcData;
import c.o.m.model.User;
import c.o.m.model.UserComponent;
import c.o.m.model.UserRegModel;


@Controller
public class AjaxController {
	
	@Autowired
	private UserComponent users;
	
	@JsonView(Views.Public.class)
	@ResponseBody
	@RequestMapping(value="/add_user", method=RequestMethod.POST)
	public String postAddUser(@RequestBody @JsonView(Views.Public.class) UserRegModel reg) {
		User user = new User();
		user.setUsername(reg.getArg_1());
		user.setPassword(reg.getArg_2());
		user.setEmail(reg.getArg_3());
		user.setPassword(reg.getArg_4());
		user.setAddress(reg.getArg_5());
		users.addUser(user);
		return "ok";
	}
	
	@JsonView(Views.Public.class)
	@ResponseBody
	@RequestMapping(value="/search_user", method= RequestMethod.POST)
	public AjaxResponseBody postSearch(@RequestBody String username) {
		
		AjaxResponseBody result = new AjaxResponseBody();
		
		if(username != null && !username.isEmpty()) {
			username = username.replace("\"", " ").trim();
			List<User> users = this.users.findUser(username);
			if(users.size() > 0) {
				result.setCode("200");
				result.setMsg("");
				result.setResult(users);
			}
			else {
				result.setCode("204");
				result.setMsg("No users!");
			}
		} else {
			result.setCode("404");
			result.setMsg("Search criteria is empty!");
		}
		return result;
	}	
	
	//
	//				OLD BELOW
	//
	
	@ResponseBody
	@RequestMapping(value="/calc_this", method= {RequestMethod.GET, RequestMethod.POST})
	public String getCalcThis(@RequestBody String inputData) {
		inputData += "asdasda asd a ".trim();
		return inputData;
	}
	
	@JsonView(Views.Public.class)
	@ResponseBody
	@RequestMapping(value="/calc", method=RequestMethod.POST)
	public int postCalc(@JsonView(Views.Public.class) @RequestBody CalcData calcData){
		return calcData.getArg_1()*2 + calcData.getArg_2();
	}
	
	@JsonView(Views.Public.class)
	@ResponseBody
	@RequestMapping(value="/do", method= {RequestMethod.GET, RequestMethod.POST})
	public AjaxResponseBody getSearchResultViaAjax(@JsonView(Views.Public.class) @RequestBody String search) {
		
		AjaxResponseBody result = new AjaxResponseBody();
		
		if(search != null && !search.isEmpty()) {
			search = search.replace("\"", " ").trim();
			List<User> users = this.users.findUser(search);
			if(users.size() > 0) {
				result.setCode("200");
				result.setMsg("");
				result.setResult(users);
			}
			else {
				result.setCode("204");
				result.setMsg("No users!");
			}
		} else {
			result.setCode("404");
			result.setMsg("Search criteria is empty!");
		}
		return result;
	}	
}
