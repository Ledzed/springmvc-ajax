package c.o.m.model;

import com.fasterxml.jackson.annotation.JsonView;

import c.o.m.Views;

public class CalcData {
	
	@JsonView(Views.Public.class)
	private int arg_1;
	@JsonView(Views.Public.class)
	private int arg_2;
	
	public CalcData() {
		super();
	}
	public CalcData(int arg_1, int arg_2) {
		super();
		this.arg_1 = arg_1;
		this.arg_2 = arg_2;
	}
	public int getArg_1() {
		return arg_1;
	}
	public void setArg_1(int arg_1) {
		this.arg_1 = arg_1;
	}
	public int getArg_2() {
		return arg_2;
	}
	public void setArg_2(int arg_2) {
		this.arg_2 = arg_2;
	}
}
