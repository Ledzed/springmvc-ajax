package c.o.m.model;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class UserComponent {
	
	List<User> users;
	
	@PostConstruct
	private void insertData() {
		users = new ArrayList<User>();
		users.add(new User("Alex","123123","Alex@epam.com","00012","Minsk, Kuprievicha 1/1"));
		users.add(new User("Bob","123123423","Bob@epam.com","00011","Minsk, Kuprievicha 1/1"));
		users.add(new User("Michael","232f343","Michael@epam.com","00010","Minsk, Kuprievicha 1/3"));
		users.add(new User("Anastasia","sfg345g4","Anastasia@epam.com","00009","Minsk, Kuprievicha 1/3"));
		users.add(new User("Gregory","eh5fere","Gregory@epam.com","00008","Minsk, Kuprievicha 1/3"));
		users.add(new User("Nick","34g35h4gfr","Nick@epam.com","00607","Minsk, Lozinskaya 4"));
		users.add(new User("Jack","trewrf34","Jack@epam.com","00006","Minsk, Lozinskaya 4"));
		users.add(new User("Helen","rwgf34f","Helen@epam.com","00007","Minsk, Lozinskaya 4"));
		users.add(new User("Sandra","g435gfg","Sandra@epam.com","10101","Minsk, Lozinskaya 4"));
		users.add(new User("Sam","t234t245","Sam@epam.com","00101","Minsk, Lozinskaya 4"));
	}
	
	public void addUser(User user) {
		users.add(user);
	}
	
	@Bean
	public List<User> findAll(){
		return users;
	}
	
	public List<User> findUser(String search){
		List<User> result = new ArrayList<User>();
		for(User user : users) {
			if(user.getUsername().toLowerCase().contains(search.toLowerCase())) {
				result.add(user);
			}
		}
		return result;
	}
}
