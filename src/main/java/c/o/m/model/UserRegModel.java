package c.o.m.model;

import com.fasterxml.jackson.annotation.JsonView;

import c.o.m.Views;

public class UserRegModel {
	
	@JsonView(Views.Public.class)
	private String arg_1;
	@JsonView(Views.Public.class)
	private String arg_2;
	@JsonView(Views.Public.class)
	private String arg_3;
	@JsonView(Views.Public.class)
	private String arg_4;
	@JsonView(Views.Public.class)
	private String arg_5;
	
	public UserRegModel() {
		super();
	}

	public UserRegModel(String arg_1, String arg_2, String arg_3, String arg_4, String arg_5) {
		super();
		this.arg_1 = arg_1;
		this.arg_2 = arg_2;
		this.arg_3 = arg_3;
		this.arg_4 = arg_4;
		this.arg_5 = arg_5;
	}

	public String getArg_1() {
		return arg_1;
	}

	public void setArg_1(String arg_1) {
		this.arg_1 = arg_1;
	}

	public String getArg_2() {
		return arg_2;
	}

	public void setArg_2(String arg_2) {
		this.arg_2 = arg_2;
	}

	public String getArg_3() {
		return arg_3;
	}

	public void setArg_3(String arg_3) {
		this.arg_3 = arg_3;
	}

	public String getArg_4() {
		return arg_4;
	}

	public void setArg_4(String arg_4) {
		this.arg_4 = arg_4;
	}

	public String getArg_5() {
		return arg_5;
	}

	public void setArg_5(String arg_5) {
		this.arg_5 = arg_5;
	}
}
